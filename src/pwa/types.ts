export interface PWAOptions {
  pwaDir: string;
  display: PWADisplayOptions;
  shortName: string;
  themeColor: string;
}
export type PWADisplayOptions =
  | "fullscreen"
  | "minimal-ui"
  | "standalone"
  | "browser";
