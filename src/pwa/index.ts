import {
  DefaultTreeDocument,
  DefaultTreeElement,
  parse as parseHtml
} from "parse5";
import { getChildElementIndentation } from "./parse5-element";
import {
  apply,
  mergeWith,
  move,
  Rule,
  SchematicContext,
  template,
  Tree,
  url,
  SchematicsException
} from "@angular-devkit/schematics";

import { normalize, strings } from "@angular-devkit/core";
import { PWAOptions } from "./types";
export function getIndexHtmlPath(): string {
  return "index.html";
}

export function getHtmlHeadTagElement(
  htmlContent: string
): DefaultTreeElement | null {
  const document = parseHtml(htmlContent, {
    sourceCodeLocationInfo: true
  }) as DefaultTreeDocument;
  const nodeQueue = [...document.childNodes];

  while (nodeQueue.length) {
    const node = nodeQueue.shift() as DefaultTreeElement;

    if (node.nodeName.toLowerCase() === "head") {
      return node;
    } else if (node.childNodes) {
      nodeQueue.push(...node.childNodes);
    }
  }

  return null;
}

// do elementu <head> wrzucamy przekazany html
export function appendHtmlElementToHead(
  tree: Tree,
  htmlFilePath: string | undefined,
  elementHtml: string
) {
  if (!htmlFilePath) {
    throw new SchematicsException(`Path to index.html not defined!`);
  }

  const htmlFileBuffer = tree.read(htmlFilePath);

  if (!htmlFileBuffer) {
    throw new SchematicsException(
      `Could not read file for path: ${htmlFilePath}`
    );
  }

  const htmlContent = htmlFileBuffer.toString();

  if (htmlContent.includes(elementHtml)) {
    return;
  }

  const headTag = getHtmlHeadTagElement(htmlContent);

  if (!headTag) {
    throw new SchematicsException(
      `Could not find '<head>' element in HTML file: ${htmlFileBuffer}`
    );
  }

  const endTagOffset = headTag!.sourceCodeLocation!.endTag.startOffset;
  const indentationOffset = getChildElementIndentation(headTag);
  const insertion = `${" ".repeat(indentationOffset)}${elementHtml}`;

  const recordedChange = tree
    .beginUpdate(htmlFilePath)
    .insertRight(endTagOffset, `${insertion}\n`);

  tree.commitUpdate(recordedChange);
}

// You don't have to export the function as default. You can also have more than one rule factory
// per file.
export function pwa(_options: PWAOptions): Rule {
  return (tree: Tree, _context: SchematicContext) => {
    console.log(_options);

    const movePath = normalize(strings.dasherize(_options.pwaDir));

    const templateSource = apply(url("./files"), [
      template({
        ...strings,
        ..._options
      }),
      move(movePath)
    ]);

    const fontLink = `<link href="${
      _options.pwaDir
    }/manifest.json" rel="manifest">`;

    appendHtmlElementToHead(tree, getIndexHtmlPath(), fontLink);

    const rule = mergeWith(templateSource);
    return rule(tree, _context);
  };
}
